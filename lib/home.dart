import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:listatarefas/Models/listaModel.dart';
import 'providers/lista.providers.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var lista2 = [];
  bool _isLoading = true;

  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    Provider.of<ListaProviders>(context, listen: false).loadLista().then((_) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  void exibeSnack(String texto) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.red,
        content: Text(texto),
        duration: Duration(seconds: 3)));
  }

  void salvar() async {
    if (_controller.text.isNotEmpty) {
      setState(() {
        _isLoading = true;
      });
      var task =
          new Lista(id: 0, descricao: _controller.text, concluido: false);
      Provider.of<ListaProviders>(context, listen: false).postTask(task).then(
          (_) {
        setState(() {
          lista2.add({
            "id": task.id,
            "descricao": task.descricao,
            "concluido": task.concluido
          });
          _isLoading = false;
        });
        _controller.text = "";
      }, onError: (err) {
        setState(() {
          _isLoading = false;
        });
        exibeSnack("Erro ao salvar a tarefa");
      });
    } else {
      exibeSnack("Campo de tarefa deve ser Preenchido!");
    }
  }

  @override
  Widget build(BuildContext context) {
    final listData = Provider.of<ListaProviders>(context);
    final list = listData.items;

    return Scaffold(
      appBar: AppBar(
        title: Text('| Lista de Tarefas |'),
        centerTitle: true,
        backgroundColor: Colors.lightGreen[900],
      ),
      body: Column(children: [
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10),
                child: TextField(
                  controller: _controller,
                  decoration: InputDecoration(
                    hintText: "Digite nova tarefa",
                  ),
                ),
              ),
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Colors.lightGreen[900]),
              ),
              child: Text('Salvar'),
              onPressed: salvar,
            )
          ],
        ),
        _isLoading
            ? Expanded(
                child: Container(
                  alignment: Alignment.bottomCenter,
                  padding: EdgeInsets.only(top: 50),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              )
            : Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: ListView.builder(
                    itemCount: listData.itemsCount,
                    itemBuilder: (ctx, index) => buildItem(context, index, list),
                  ),
                ),
              ),
      ]),
    );
  }

  Widget buildItem(BuildContext context, int index, List<Lista> list) {
    return Dismissible(
      key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
      background: Container(
        color: Colors.red,
        child: Align(
          alignment: Alignment(-0.9, 0),
          child: Icon(
            Icons.delete,
            color: Colors.white,
          ),
        ),
      ),
      direction: DismissDirection.startToEnd,
      child: SingleChildScrollView(
        child: CheckboxListTile(
          value: list[index].concluido,
          title: Text(
            "- ${list[index].descricao}",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          secondary: CircleAvatar(
            child: Icon(list[index].concluido ? Icons.check : Icons.error),
          ),
          onChanged: (value) {
            setState(() {
              list[index].concluido = value!;
              Provider.of<ListaProviders>(context, listen: false)
                  .updateTask(list[index]);
            });
          },
        ),
      ),
      onDismissed: (direction) {
        Provider.of<ListaProviders>(context, listen: false)
            .deleteTask(list[index].id);
        list.removeAt(index);
      },
    );
  }
}
