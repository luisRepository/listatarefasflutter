import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:listatarefas/home.dart';
import 'package:provider/provider.dart';

import 'providers/lista.providers.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => new ListaProviders(),
        )
      ],
      child: MaterialApp(
      title: "Lista de tarefas",
      theme: ThemeData(
        hintColor: Colors.black,
      ),
      home: Home(),
    ));
  }
}