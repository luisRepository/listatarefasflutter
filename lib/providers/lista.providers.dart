import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:listatarefas/Models/listaModel.dart';

class ListaProviders with ChangeNotifier {
  List<Lista> _items = [];
  List<Lista> get items => [..._items];

  Future loadLista() async {
    var response = await http.get("http://192.168.1.67:3000/tarefas");
    List<dynamic> data = json.decode(response.body);
    data.forEach((categoryData) {
      _items.add(Lista(
          id: categoryData['id'],
          descricao: categoryData['descricao'],
          concluido: categoryData['concluido']));
    });
    notifyListeners();

    return Future.value();
  }

  int get itemsCount {
    return _items.length;
  }

  Future postTask(Lista task) async {
    try {
      var object = json.encode({
        "id": _items.length + 1,
        "descricao": task.descricao,
        "concluido": task.concluido
      });
      await http.post("http://localhost:3000/tarefas",
          headers: {"Content-Type": "application/json"}, body: object);
      _items.add(Lista(
          id: (_items.length + 1),
          descricao: task.descricao,
          concluido: task.concluido));
      notifyListeners();
      return Future.value();
    } catch (e) {
      print(e);
    }
  }

  Future deleteTask(int id) async {
    try {
      await http.delete("http://localhost:3000/tarefas/$id");
      _items.removeWhere((element) => element.id == id);
      notifyListeners();
      return Future.value();
    } catch (e) {
      print(e);
    }
  }

  Future updateTask(Lista task) async {
    try {
      var object = json.encode({
        "id": task.id,
        "descricao": task.descricao,
        "concluido": task.concluido
      });
      await http.put("http://localhost:3000/tarefas/${task.id}",
          headers: {"Content-Type": "application/json"}, body: object);
      notifyListeners();
      return Future.value();
    } catch (e) {
      print(e);
    }
  }
}
