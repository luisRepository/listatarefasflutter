class Lista {
  final int id;
  final String descricao;
  bool concluido;

  Lista({required this.id, required this.descricao, required this.concluido});
}
